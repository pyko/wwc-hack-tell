var five = require('johnny-five');
var express = require('express');
var config = require('./config');
var app = express();

app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

app.use('/favicon.ico', express.static('public/images/favicon.ico'));
app.use(express.static('public'));


var startServer = function () {
  app.listen(3000, function() {
    console.log('Running site on http://localhost:3000')
  });

  app.get('/', function (req, res) {
    res.render('index')
  });
};

if (config.USE_ARDUINO) {
  // See config.js for ARDUINO_NAME
  console.log('Attempting to connect to: ' + config.ARUDINO_NAME);
  var board = new five.Board({
    port: config.ARUDINO_NAME
  });

  try {
    // The board's pins will not be accessible until
    // the board has reported that it is ready
    board.on("ready", function() {
      // Start the server after board is ready so we know we can manipulate the pins
      console.log('Successfully connected to board');
      startServer();

      require('./arduino')(app);

    }).on("fail", function (err) {})
      .on("error", function (err) {});
  } catch(err) {
    // Something bad happened...give error
    console.log("Error connecting to Arduino/Bluetooth");
    console.log(err);
  }
} else {
  // Ignore arduino, just start server
  console.log('Starting server without arduino');
  startServer();
}
