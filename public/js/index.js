$(document).ready(function () {
  var ACTIONS = {
    'on'    : { 'action': '/on',    'newText': 'Off'},
    'off'   : { 'action': '/off',   'newText': 'On' },
    'flash' : { 'action': '/flash', 'newText': 'Stop' },
    'stop'  : { 'action': '/stop',  'newText': 'Flash' }
  }
  $('button').on('click', function () {
    var $button = $(this);
    var intent = $button.text().toLowerCase();
    fetch(ACTIONS[intent].action, {method: 'POST'})
      .then(function (resp) {
        if (resp.status >= 400) {
          throw new Error('Non 200 response');
        }
        $button.text(ACTIONS[intent].newText);
        console.log('State: ' + intent);
      }).catch(function (e) {
        console.log('An error occurred: ', e);
      });
  });

  var light = $('#light');
  var numFailures = 0;
  var intervalId = setInterval(function () {
    fetch('/isLightOn')
      .then(function (resp) {
        if (resp.status != 200) {
          throw new Error('Unable to get state of light');
        }
        return resp.json();
      }).then(function (result) {
        if (result.isLightOn) {
          light.addClass('on');
        } else {
          light.removeClass('on');
        }
      }).catch(function (e) {
        console.log('An error occurred: ', e);
        numFailures++;
        if (numFailures >= 10) {
          console.log('Assuming server was closed, stop attempting to fetch light status');
          clearInterval(intervalId);
        }
      });
  }, 50);
});
