
module.exports = {
  // Name will likely be different
  // Arduino IDE -> Tools -> Port or npm run list-ports
  // It will be something like:
  //  Unix/Mac: /dev/tty.SOME_NAME-DevB
  //  Windows:  COMX (where X is the number of the port)
  ARUDINO_NAME: '/dev/cu.POWER-DevB',

  USE_ARDUINO: true
};
