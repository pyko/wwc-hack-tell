# Get the site running
Make sure you've done the "Initial set up" (see below).

To run the site: `npm start`

### Directories/files of interest

**arduino.js**

* Code that talks to the arduino
* [http://johnny-five.io/api/](http://johnny-five.io/api/) will be helpful

**public/\* and views/\***

* The UI of the webapp
* Similar to normal node/express apps

**config.js**

* Place to put name of bluetooth device

**get_bt_info.ino**

* File to upload to Arduino (during setup) to get/set bluetooth info

### For dev/testing

For quick dev/testing, run `npm run quick-dev` and modify the contents of `quick-dev.js` (under `extras` directory).

This should still connect to the bluetooth, but will run the trigger code immediately. You will also have the repl to play around with to quickly check things.


# Initial set up

This is to set up the bluetooth with the laptop. These steps should only need to be done once.

1. Follow: [https://github.com/rwaldron/johnny-five/wiki/Getting-Started-with-Johnny-Five-and-HC-05-Bluetooth-Serial-Port-Module](https://github.com/rwaldron/johnny-five/wiki/Getting-Started-with-Johnny-Five-and-HC-05-Bluetooth-Serial-Port-Module)

1. Pair laptop with HC-05

1. Install Node/npm

1. Go into directory and run `npm install`

1. Run `npm run list-ports` to see the name of the the board and update `config.js`
