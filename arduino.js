var five = require('johnny-five');

/*
 *    Code to control the Arduino board
 *    See http://johnny-five.io/api/ for API
 */
var arduino = function (app) {
  // Get hold PIN 12
  var pin = new five.Pin(12);
  var intervalId;

  app.post('/on', function (req, res) {
     // Send high to pin
     pin.high();

     res.status(202).send();
  });

  app.post('/off', function (req, res) {
     // Send low to pin
     pin.low();

     res.status(202).send();
  });

  app.post('/flash', function (req, res) {
     intervalId = setInterval(function () {
       pin.write(!pin.value);
     }, 1000);

     res.status(202).send();
  });

  app.post('/stop', function (req, res) {
    clearInterval(intervalId);
    pin.low();

    res.status(202).send();
  });

  app.get('/isLightOn', function (req, res) {
    res.json({isLightOn: pin.value});
  });
};

module.exports = arduino;
