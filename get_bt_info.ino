// https://github.com/rwaldron/johnny-five/wiki/Getting-Started-with-Johnny-Five-and-HC-05-Bluetooth-Serial-Port-Module

#include <SoftwareSerial.h>

#define BT_NAME "SomeName"
// use 57600 if re-uploading (this script changes the baudrate)
#define BLUETOOTH_SPEED 38400


//   PIN 9  --> Bluetooth EN
//   Pin 10 --> Bluetooth TX
//   Pin 11 --> Bluetooth RX

SoftwareSerial mySerial(10, 11); // RX, TX

void setup() {
  pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  digitalWrite(9, HIGH);
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  Serial.println("Starting config");
  mySerial.begin(BLUETOOTH_SPEED);
  delay(1000);

  // Should respond with OK
  mySerial.print("AT\r\n");
  waitForResponse();

  mySerial.print("AT+VERSION\r\n");
  waitForResponse();

  mySerial.print("AT+PSWD?\r\n");
//  mySerial.print("AT+PSWD=0000\r\n"); // set PIN to 0000
  waitForResponse();

  mySerial.print("AT+NAME?\r\n");
  waitForResponse();

  mySerial.print("AT+IPSCAN?\r\n");
  waitForResponse();

  mySerial.print("AT+ROLE?\r\n");
  waitForResponse();

  // Set role to Slave
  // 0: Slave, 1: Master
  //mySerial.print("AT+ROLE=0\r\n");
  //waitForResponse();

  mySerial.print("AT+ADDR\r\n");
  waitForResponse();

  // Set the name to BT_NAME
//  String rnc = String("AT+NAME=") + String(BT_NAME) + String("\r\n"); 
//  mySerial.print(rnc);
//  waitForResponse();

// Set baudrate to 57600
  mySerial.print("AT+UART=57600,0,0\r\n");
  waitForResponse();


  Serial.println("Done!");
}

void waitForResponse() {
    delay(1000);
    while (mySerial.available()) {
      Serial.write(mySerial.read());
    }
    Serial.write("\n");
}

void loop() {}
