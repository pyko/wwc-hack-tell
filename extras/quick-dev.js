var five = require('johnny-five');
var config = require('../config');

// Name will likely be different
// Arduino IDE -> Tools -> Port
// It will be something like:
//  Unix/Mac: /dev/tty.SOME_NAME-DevB
//  Windows:  /dev/tty.SOME_NAME-COMX (where X is the number of the port)
console.log('Attempting to connect to: ' + config.ARUDINO_NAME);
var board = new five.Board({
  port: config.ARUDINO_NAME
});


try {
  // The board's pins will not be accessible until
  // the board has reported that it is ready
  board.on("ready", function() {
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *    Code to control the Arduino board                  *
     *    See http://johnny-five.io/api/ for API             *
     *                                                       *
     *    MODIFY CODE BELOW                                  *
     *   v v v v v v v v v v v v v v v v v v v v v v v v v   *
     */

     // Get hold of pin 12
     var pin = new five.Pin(12);
     var intervalId;

     // Injecting the repl allows you to test things out in the repl (command line)
     // This is useful for dev, but probably shouldn't be used for the real thing
     this.repl.inject({
       // Allow shortcut on/off control access to the led instance from the REPL.
       on: function() {
         pin.high();
       },
       off: function() {
         pin.low();
       },
       flash: function () {
         intervalId = setInterval(function () {
           pin.write(!pin.value);
         }, 1000);
       },
       stop: function () {
         clearInterval(intervalId);
       },

       // Allow access to led from repl
       // Gives access to the led api witout explictly exposing them
       pin: pin
     });

    /*   ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^   *
     *    MODIFY CODE ABOVE                                  *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  }).on("fail", function (err) {})
    .on("error", function (err) {});
} catch(err) {
  // Something bad happened...still start server, but give error
  console.log("Error connecting to Arduino/Bluetooth");
  console.log(err);
}
