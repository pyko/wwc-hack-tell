var serialport = require("serialport");


serialport.list(function(err, ports) {
  console.log('\nBluetooth ports:')
  console.log('------------');
  ports.forEach(function (port) {
    console.log('Name: ' + port.comName);
    console.log('------------');
  });
  console.log('\n');
});
